import {createStore ,combineReducers}   from 'redux';
import { operationsReducer } from "./operations";

export const store = createStore(
    combineReducers({operationsReducer})
)