const initialState =[
    {id:134143,todo:"awsrt",completed:true},
    {id:132414,todo:"awsrtdsf",completed:false},
    {id:1122414,todo:"aasf",completed:false},
    
]

export const operationsReducer=(state=initialState, action)=>{
    switch(action.type){
        case 'ADD_TODO':
            return [...state, action.payload];
        case 'DELETE_ALL':
            return [];
        case 'REMOVE_TODO': 
            const filteredTodos = state.filter((todo)=>todo.id!==action.payload.id);
            return filteredTodos;
        case 'UPDATE_TODO':
            let data = action.payload;  
            const updatedArray=[];
            state.forEach(item => {
                if(item.id===data.id){
                    item.id = data.id;
                    item.todo = data.todo;
                    item.completed = data.completed;
                }
                updatedArray.push(item);
            });

            return updatedArray;
            case 'UPDATE_CHECK':
                let todoArray=[];
                state.forEach(item=>{
                    if(item.id===action.payload.id){
                        item.completed = !item.completed;
                    }
                    todoArray.push(item);
                })
                return todoArray;
        
        default: return state;
    }
}