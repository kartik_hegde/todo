import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Typography, Button, Flex } from "antd";
import Forms from "./components/Forms";
import Todos from "./components/Todos";

function App() {
  const { Title } = Typography;

  const dispatch = useDispatch();
  const todos = useSelector((state) => state.operationsReducer);

  //edit form ...
  const [editFormVisibility, setEditFormVisibility] = useState(false);
  const [editTodo, setEditTodo] = useState("");

  const handleEditClick = (todo) => {
    setEditFormVisibility(true);
    setEditTodo(todo);
  };

  // back button
  const cancelUpdate = () => {
    setEditFormVisibility(false);
  };

  return (
    <div>
      <Title
        style={{
          color: "black",
          fontSize: "50px",
          textDecoration: "underline",
        }}
      >
        TODO-APP USING REACT-REDUX
      </Title>
      <Forms
        editFormVisibility={editFormVisibility}
        editTodo={editTodo}
        cancelUpdate={cancelUpdate}
      />
      <Flex wrap="wrap" gap="small">
        <Todos
          handleEditClick={handleEditClick}
          editFormVisibility={editFormVisibility}
        />
      </Flex>

      {todos.length > 1 ? (
        <Button
          className="wrapper" 
          type="primary"
          danger
          onClick={() =>
            dispatch({
              type: "DELETE_ALL",
            })
          }
        >
          {" "}
          DELETE ALL
        </Button>
      ) : null}
    </div>
  );
}

export default App;
