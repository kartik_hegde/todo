import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { DeleteFilled, EditFilled } from "@ant-design/icons";
import { Checkbox , Space } from "antd";
const Todos = (props) => {
  const { handleEditClick, editFormVisibility } = props;
  const dispatch = useDispatch();

  const todos = useSelector((state) => state.operationsReducer);
  return todos.map((todo) => (
    <div key={todo.id} className="todo-box">
      <div className="content">
        {editFormVisibility === false && (
          <Checkbox
            checked={todo.completed}
            onChange={() => {
              dispatch({
                type: "UPDATE_CHECK",
                payload: todo,
              });
            }}
          ></Checkbox>  
        )}
        <div className="para">
          <p
            style={
              todo.completed === true
                ? { textDecoration: "line-through" }
                : { textDecoration: "none" }
            }
          >
            {todo.todo}
          </p>
        </div>
      </div>
      <div className="actions-box">
        {editFormVisibility === false && (
          <>
            <Space className="icons" onClick={() => handleEditClick(todo)}>
              <EditFilled />
            </Space>
            <Space
              className="icons"
              onClick={() => dispatch({ type: "REMOVE_TODO", payload: todo })}
            >
              <DeleteFilled />
            </Space>
          </>
        )}
      </div>
    </div>
  ));
};
export default Todos;
